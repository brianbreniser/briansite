// (Lines like the one below ignore selected Clippy rules
//  - it's useful when you want to check your code with `cargo make verify`
// but some rules are too "annoying" or are not applicable for your case.)
#![allow(clippy::wildcard_imports)]
use rand::Rng;
use seed::{prelude::*, *};

// use std::time::{Instant};

// ------ ------
//     Init
// ------ ------

const DEFAULT_REPS: u32 = 1;

// `init` describes what should happen when your app started.
fn init(_: Url, _: &mut impl Orders<Msg>) -> Model {
    Model {
        // Lifting Table
        starting_weight: 0,

        // 1rm calc
        the_weight: 0,
        the_reps: DEFAULT_REPS,
        estimated_one_rep_max: 0,

        // RPE
        rpe_weight: 0,

        // Conways game of life
        conway_canvas_width: 340,
        conway_canvas_height: 340,
        conway_universe_width: 34,
        conway_universe_height: 34,
        conway_cell_size: 9,
        conway_running: false,

        // Todo
        todos: vec![],
        new_todo_title: "".to_string(),

        // Button chooser
        chosen1: false,
        chosen2: false,
        chosen3: false,

        // fib
        num: 0,
        fib_num: 0,
    }
}

// ------ ------
//     Model
// ------ ------

// `Model` describes our app state.
struct Model {
    // Just errors
    // error: String,

    // Lifting Table
    starting_weight: u16,

    // Strength training stuff
    the_weight: u32,
    the_reps: u32,
    estimated_one_rep_max: u32,

    // RPE
    rpe_weight: u16,

    // Conways game of life
    conway_canvas_width: u32,
    conway_canvas_height: u32,
    conway_universe_width: u32,
    conway_universe_height: u32,
    conway_cell_size: u32,
    conway_running: bool,

    // App_todo lists
    todos: Vec<Todo>,
    new_todo_title: String,

    // Chooser
    chosen1: bool,
    chosen2: bool,
    chosen3: bool,

    // Fib
    num: i64,
    fib_num: i64,
}

// ------ ------
//     Supporting Models
// ------ ------

struct Todo {
    id: u64,
    title: String,
    completed: bool,
}

enum Msg {
    // Lifting Table
    CalculateNewWeightTable(String),
    // log
    ConsoleLog,

    // 1rm calc
    WeightUpdated(String),
    RepsUpdated(String),

    // rpe
    RpeWeightUpdated(String),

    // Conways game of life
    DrawGrid,
    ConwayRandom,
    ConwayStep,
    ConwayToggleSim,

    // todo
    NewTodoTitleChanged(String),
    CreateTodo,
    ToggleTodoCompleted(u64),
    RemoveDoneTodos,

    // button chooser
    Choose1,
    Choose2,
    Choose3,

    // fib
    FibChange,
}

// Conways game of life
#[derive(Copy, Clone, Debug, PartialEq)]
enum Tile {
    EMPTY,
    ALIVE,
}

// `update` describes how to handle each `Msg`.
fn update(msg: Msg, model: &mut Model, _: &mut impl Orders<Msg>) {
    match msg {
        // Some debugging
        Msg::ConsoleLog => log!["You typed a key in the right place"],

        // Lifting Table
        Msg::CalculateNewWeightTable(s) => {
            model.starting_weight = s.parse::<u16>().unwrap_or_else(|_| 0);
        }

        // Weight lifting
        Msg::WeightUpdated(s) => {
            // log!("Hey you used non-u32 characters")
            // model.error = "".to_string();
            model.the_weight = s.parse::<u32>().unwrap_or_else(|_| {
                // model.error = "You entered the wrong data in the Enter Weight field".to_string();
                0
            });

            calculate_new_weight(model);
        }

        Msg::RepsUpdated(s) => {
            // log!("Hey you used non-u32 characters")
            // model.error = "".to_string();
            model.the_reps = s.parse::<u32>().unwrap_or_else(|_| {
                // model.error = "You entered the wrong data in the Enter Weight field".to_string();
                DEFAULT_REPS
            });

            calculate_new_weight(model);
        }

        Msg::RpeWeightUpdated(s) => {
            model.rpe_weight = s.parse::<u16>().unwrap_or_else(|_| {
                // model.error = "You entered the wrong data in the Enter Weight field".to_string();
                0
            });
        }

        // Conways game of life
        Msg::DrawGrid => {
            draw_grid(model);
        }

        Msg::ConwayRandom => {
            // conway_test_alive(model);
            conway_random(model);
        }

        Msg::ConwayStep => {
            log!("You stepped the simulation!");
        }

        Msg::ConwayToggleSim => {
            if model.conway_running {
                model.conway_running = false;
            } else {
                model.conway_running = true;
            }
        }

        // app_todo
        Msg::CreateTodo => {
            model.todos.push({
                let mut rng = rand::thread_rng();
                let id = rng.gen::<u64>();
                Todo {
                    id,
                    title: model.new_todo_title.clone(),
                    completed: false,
                }
            });
            model.new_todo_title = "".to_string();
        }
        Msg::NewTodoTitleChanged(s) => {
            model.new_todo_title = s;
        }
        Msg::ToggleTodoCompleted(id) => {
            let inner = model.todos.iter_mut().find(|t| t.id == id).unwrap();

            inner.completed = !inner.completed;
        }
        Msg::RemoveDoneTodos => {
            model.todos.retain(|t| !t.completed);
        }

        // Choser
        Msg::Choose1 => {
            model.chosen1 = !model.chosen1;
            model.chosen2 = false;
            model.chosen3 = false
        }
        Msg::Choose2 => {
            model.chosen1 = false;
            model.chosen2 = !model.chosen2;
            model.chosen3 = false
        }
        Msg::Choose3 => {
            model.chosen1 = false;
            model.chosen2 = false;
            model.chosen3 = !model.chosen3;
        }

        // Fib
        Msg::FibChange => {
            model.num += 1;
            fib(model);
        }
    }
}

// ------ -----
//   Helpers
// ------ -----
fn calculate_new_weight(model: &mut Model) {
    // (weight * reps * 0.033) + weight
    model.estimated_one_rep_max =
        ((f64::from(model.the_weight) * f64::from(model.the_reps as f64) * 0.033_f64)
            + f64::from(model.the_weight)) as u32;
    model.starting_weight = model.estimated_one_rep_max as u16;
    model.rpe_weight = model.estimated_one_rep_max as u16;
}

fn fib(model: &mut Model) {
    println!("ran fib");

    // fib(92) returns some crazy runtime error. Possibly a webassembly thing. fib(90) is enough to prove a point
    if model.num >= 90 {
        model.fib_num = 0;
        return;
    }

    if model.num <= 2 {
        model.fib_num = 1;
        return;
    }

    let mut data = [1, 1];

    for _ in 3..=model.num {
        let tmp = data[0] + data[1];
        data[0] = data[1];
        data[1] = tmp;
    }

    model.fib_num = data[1];
}

// ------ -----
//     View
// ------ ------
fn view(model: &Model) -> Node<Msg> {
    div![
        h1!["This is the main page, Hello. Here are some apps"],
        // IF![!model.error.is_empty() => p!["Error: ", &model.error]],
        p![
            "This page was made with ",
            a![
                attrs![At::from("href") => "https://seed-rs.org/"],
                "Seed-rs",
            ]
        ],
        app_find_max(model),
        app_weights_table(model),
        app_rpe(model),
        app_game_of_life(model),
        app_todo(model),
        app_chooser(model),
        app_fib(model),
    ]
}

// ------ -----
//     Application views to be used in the view section
// ------ ------
fn make_app_container() -> seed::Style {
    style! {
        St::BorderStyle =>  "solid",
        St::BorderColor => "#111111",
        St::BackgroundColor => "#333333",
        St::BorderRadius => rem(1.0),
        St::BorderWidth => rem(0.05),
        St::Padding => rem(1),
        St::Margin => rem(1),
        St::OverflowWrap => "anywhere",
    }
}

fn app_weights_table(model: &Model) -> Node<Msg> {
    // TODO: add css -> overflow-wrap: anywhere to parent container
    div![
        make_app_container(),
        h2!["Calculate percentages based on max"],
        input![
            attrs![
                At::Placeholder => "Enter max",
                At::AutoFocus => AtValue::None,
            ],
            input_ev(Ev::Input, Msg::CalculateNewWeightTable)
        ],
        div!["Maxes table:"],
        table![make_headers(), make_table(model.starting_weight, false),],
        div!["Training maxes table:"],
        table![make_headers(), make_table(model.starting_weight, true),],
    ]
}

fn make_headers() -> Node<Msg> {
    return tr![
        th!["40%"],
        th!["45%"],
        th!["50%"],
        th!["55%"],
        th!["60%"],
        th!["65%"],
        th!["70%"],
        th!["75%"],
        th!["80%"],
        th!["85%"],
        th!["90%"],
        th!["95%"],
        th!["100%"],
    ];
}

fn make_table(w: u16, training_max: bool) -> Node<Msg> {
    return tr![
        td![round_weight(w, 40f32, training_max)],
        td![round_weight(w, 45f32, training_max)],
        td![round_weight(w, 50f32, training_max)],
        td![round_weight(w, 55f32, training_max)],
        td![round_weight(w, 60f32, training_max)],
        td![round_weight(w, 65f32, training_max)],
        td![round_weight(w, 70f32, training_max)],
        td![round_weight(w, 75f32, training_max)],
        td![round_weight(w, 80f32, training_max)],
        td![round_weight(w, 85f32, training_max)],
        td![round_weight(w, 90f32, training_max)],
        td![round_weight(w, 95f32, training_max)],
        td![round_weight(w, 100f32, training_max)],
    ];
}

fn app_rpe(model: &Model) -> Node<Msg> {
    div![
        make_app_container(),
        h2!["Calculate your RPE table:"],
        input![
            attrs![
                At::Placeholder => "Enter Weight",
                At::AutoFocus => AtValue::None,
            ],
            input_ev(Ev::Input, Msg::RpeWeightUpdated),
        ],
        div!["RPE table:"],
        table![
            rpe_headers(),
            rpe_table(1, model.rpe_weight, false),
            rpe_table(2, model.rpe_weight, false),
            rpe_table(3, model.rpe_weight, false),
            rpe_table(4, model.rpe_weight, false),
            rpe_table(5, model.rpe_weight, false),
            rpe_table(6, model.rpe_weight, false),
            rpe_table(7, model.rpe_weight, false),
            rpe_table(8, model.rpe_weight, false),
        ],
    ]
}

fn rpe_headers() -> Node<Msg> {
    return tr![
        th!["RPE"],
        th!["10"],
        th!["9"],
        th!["8"],
        th!["7"],
        th!["6"],
    ];
}

fn rpe_table(rpe_row: usize, w: u16, training_max: bool) -> Node<Msg> {
    let rpe_rainbow = [
        100f32, 95.5, 92.9, 89.2, 86.3, 83.7, 81.1, 78.6, 76.2, 73.7, 70.7, 68.0,
    ];

    let mut table = tr![];
    let row = format!("{} reps", rpe_row);
    table.add_child(td![row]);
    for i in rpe_row - 1..rpe_row + 4 {
        table.add_child(td![round_weight(w, rpe_rainbow[i], training_max)]);
    }

    return table;
}

fn round_weight(weight: u16, percentage: f32, training_max: bool) -> u32 {
    let w = f32::from(weight); // weight in float
    let p = f32::from(percentage); // percentage as a float out of 100
    let mut base = w * (p / 100.0);
    if training_max {
        base = base * 0.9;
    }

    // Rounded happens not just to the nearest integer, but to the nearest 5.
    // 5, because weight plates usually round off to 5 for a set of 2.5.
    // * by 0.1 moves decimal to the left, then
    // .round() removes that rightmost digit.
    // We divide by 0.1 to get the original back, but with the rightmost digit being 0.
    // We do the same thing by multiplying by 2, rounding, then dividing by 2. This will allow
    // 5's in the rightmost digit when rounding to that number makes sense
    // 227 * 0.1 * 2.0 -> 45.39, rounded is 45, then 45 / 2 / 0.1 -> 225. so 227 rounded to 225, perfect!
    let rounded = (base * 0.1 * 2.0).round() / 2.0 / 0.1;
    return rounded as u32;
}

fn app_game_of_life(_model: &Model) -> Node<Msg> {
    div![
        make_app_container(),
        div![
            h1!["Conway's game of life"],
            button![
                id!("create_space"),
                ev(Ev::Click, |_| Msg::DrawGrid),
                "Make Grid"
            ],
            button![
                id!("random"),
                ev(Ev::Click, |_| Msg::ConwayRandom),
                "Random"
            ]
        ], div![
            button![
                id!("conway_step"),
                ev(Ev::Click, |_| Msg::ConwayStep),
                "Step"
            ],
            button![
                id!("conway_play_pause"),
                ev(Ev::Click, |_| Msg::ConwayToggleSim),
                "Play/Pause"
            ]
        ],
        div![canvas![id!("game-of-life-canvas")]]
    ]
}

fn draw_grid(model: &mut Model) {
    let canvas = canvas("game-of-life-canvas").unwrap();
    canvas.set_width(model.conway_canvas_width);
    canvas.set_height(model.conway_canvas_height);
    let ctx = canvas_context_2d(&canvas);
    ctx.begin_path();
    ctx.set_stroke_style(&JsValue::from_str("black"));

    // Vertical lines
    for i in 0..=model.conway_universe_width {
        ctx.move_to(
            (i * (model.conway_cell_size + 1)).into(),
            0.0
        );
        ctx.line_to(
            (i * (model.conway_cell_size + 1)).into(),
            model.conway_canvas_height.into()
        );
    }

    // Horizontal lines
    for i in 0..=model.conway_universe_height {
        ctx.move_to(
            0.0,
            (i * (model.conway_cell_size + 1)).into()
        );
        ctx.line_to(
            model.conway_canvas_width.into(),
            (i * (model.conway_cell_size + 1)).into()
        );
    }

    ctx.stroke();
}

fn conway_random(model: &mut Model) {
    let mut rng = rand::thread_rng();
    for _ in 0..=100 {
        let x = rng.gen::<u32>() % (model.conway_universe_width);
        let y = rng.gen::<u32>() % (model.conway_universe_height);
        conway_place_single_alive(model, x, y);
    }
}

fn conway_test_alive(model: &mut Model) {
    conway_place_single_alive(model, 0, 0);
    conway_place_single_alive(model, 5, 5);
    conway_place_single_alive(model, 10, 5);
    conway_place_single_alive(model, 15, 5);
    conway_place_single_alive(model, 20, 5);
    conway_place_single_alive(model, 25, 5);
    conway_place_single_alive(model, 30, 5);
    conway_place_single_alive(model, 35, 5);
    conway_place_single_alive(model, 5, 10);
    conway_place_single_alive(model, 5, 15);
    conway_place_single_alive(model, 5, 20);
    conway_place_single_alive(model, 5, 25);
    conway_place_single_alive(model, 5, 30);
    conway_place_single_alive(model, 5, 33);
    conway_place_single_alive(model, 33, 33);
}

fn conway_place_single_alive(model: &mut Model, x: u32, y: u32) {
    let pixel_x = x * (model.conway_cell_size + 1) + 1;
    let pixel_y = y * (model.conway_cell_size + 1) + 1;

    draw_box(
        model.conway_cell_size - 1,
        pixel_x,
        pixel_y,
        "game-of-life-canvas",
        "green"
    )

}

fn draw_box(size: u32, x: u32, y: u32, canvas_name: &str, color: &str) {
    let canvas = canvas(canvas_name).unwrap();
    let ctx = canvas_context_2d(&canvas);

    ctx.begin_path();
    ctx.set_stroke_style(&JsValue::from_str(color));
    ctx.set_fill_style(&JsValue::from_str(color));
    ctx.fill_rect(x as f64, y as f64, size as f64, size as f64);
    // ctx.fill_rect(10.0, 10.0, 9.0, 9.0);
}

fn app_find_max(model: &Model) -> Node<Msg> {
    div![
        make_app_container(),
        h2!["Calculate your max lift:"],
        input![
            attrs![
                At::Placeholder => "Enter Weight",
                At::AutoFocus => AtValue::None,
            ],
            input_ev(Ev::Input, Msg::WeightUpdated),
        ],
        input![
            attrs![
                At::Placeholder => "Enter Reps",
                At::AutoFocus => AtValue::None,
            ],
            input_ev(Ev::Input, Msg::RepsUpdated),
        ],
        p![model.the_weight, " At ", model.the_reps, " reps"],
        p!["Your estimated weight is: ", model.estimated_one_rep_max]
    ]
}

fn app_todo(model: &Model) -> Node<Msg> {
    div![
        make_app_container(),
        h2!["Todo App"],
        new_todo_input(&model.new_todo_title),
        app_todo_make_items(&model.todos),
        clear_all_todos(),
    ]
}

fn new_todo_input(title: &str) -> Node<Msg> {
    input![
        attrs![
            At::Placeholder => "What needs to be done?",
            At::AutoFocus => AtValue::None,
            At::Value => title, // Will clear input box when done
        ],
        ev(Ev::KeyPress, |_| Msg::ConsoleLog),
        input_ev(Ev::Input, Msg::NewTodoTitleChanged), // Removes takes away ability to add input
        keyboard_ev(Ev::KeyDown, |keyboard_event| {
            // Obviously enter key for adding todo
            IF!(keyboard_event.key() == "Enter" => Msg::CreateTodo)
        }),
    ]
}

fn app_todo_make_items(todos: &[Todo]) -> Node<Msg> {
    ul![
        style![
            St::from("list-style-type") => "none"
        ],
        todos.iter().map(|todo| {
            let id = todo.id;
            li![div![div![
                style![
                    St::Padding => rem(1),
                    St::BorderStyle => "solid",
                    St::BorderColor => "#111111",
                    St::BorderRadius => rem(1),
                    St::BorderWidth => rem(0.05),
                    St::Margin => rem(0.25)
                    St::BackgroundColor => if todo.completed { "grey" } else { "purple" },
                ],
                label![&todo.title],
                // ev(Ev::Click, |_| Msg::ConsoleLog),
                ev(Ev::Click, move |_| Msg::ToggleTodoCompleted(id))
            ],]]
        })
    ]
}

fn clear_all_todos() -> Node<Msg> {
    div![button![
        style! {
            St::BackgroundColor => "grey"
        },
        "Clear all Done Todos",
        ev(Ev::Click, |_| Msg::RemoveDoneTodos)
    ]]
}

fn app_chooser(model: &Model) -> Node<Msg> {
    div![
        make_app_container(),
        h2!["Button chooser"],
        button![
            style! {
                St::BackgroundColor => if model.chosen1 { "purple" } else { "grey" },
            },
            "One",
            ev(Ev::Click, |_| Msg::Choose1)
        ],
        button![
            style![
                St::BackgroundColor => if model.chosen2 { "purple" } else { "grey" },
            ],
            "Two",
            ev(Ev::Click, |_| Msg::Choose2)
        ],
        button![
            style![
                St::BackgroundColor => if model.chosen3 { "purple" } else { "grey" },
            ],
            "Three",
            ev(Ev::Click, |_| Msg::Choose3)
        ],
    ]
}

fn app_fib(model: &Model) -> Node<Msg> {
    div![
        make_app_container(),
        h2!["A fib counter, this runs quite quickly! (Only up to 90, because bugs...)"],
        button![model.num, ev(Ev::Click, |_| Msg::FibChange),],
        "<-- fib of that number is: ",
        model.fib_num,
    ]
}

// ------ ------
//     Start
// ------ ------
#[wasm_bindgen(start)]
pub fn start() {
    // Mount the `app` to the element with the `id` "app".
    App::start("app", init, update, view);
}

// ---------- ------------
//     Just the tests
// ---------- ------------

mod tests {
    #![allow(unused_imports)]
    use crate::{fib, round_weight, Model, DEFAULT_REPS};

    #[allow(dead_code)]
    fn setup() -> Model {
        Model {
            // error: "".to_string(),
            num: 0,
            fib_num: 0,
            chosen1: false,
            chosen2: false,
            chosen3: false,
            todos: vec![],
            new_todo_title: "".to_string(),
            the_weight: 0,
            the_reps: DEFAULT_REPS,
            estimated_one_rep_max: 0,
            rpe_weight: 0,
            starting_weight: 0,
            conway_canvas_width: 0,
            conway_canvas_height: 0,
            conway_universe_height: 0,
            conway_universe_width: 0,
            conway_cell_size: 3,
            conway_running: false,
        }
    }

    #[test]
    fn fib1() {
        let mut m = setup();
        m.num = 1;
        fib(&mut m);
        assert_eq!(m.fib_num, 1);
    }
    #[test]
    fn fib2() {
        let mut m = setup();
        m.num = 2;
        fib(&mut m);
        assert_eq!(m.fib_num, 1);
    }
    #[test]
    fn fib3() {
        let mut m = setup();
        m.num = 3;
        fib(&mut m);
        assert_eq!(m.fib_num, 2);
    }
    #[test]
    fn fib4() {
        let mut m = setup();
        m.num = 4;
        fib(&mut m);
        assert_eq!(m.fib_num, 3);
    }
    #[test]
    fn fib5() {
        let mut m = setup();
        m.num = 5;
        fib(&mut m);
        assert_eq!(m.fib_num, 5);
    }
    #[test]
    fn fib6() {
        let mut m = setup();
        m.num = 6;
        fib(&mut m);
        assert_eq!(m.fib_num, 8);
    }
    #[test]
    fn fib7() {
        let mut m = setup();
        m.num = 7;
        fib(&mut m);
        assert_eq!(m.fib_num, 13);
    }
    #[test]
    fn fib90() {
        let mut m = setup();
        m.num = 90;
        fib(&mut m);
        assert_eq!(m.fib_num, 0);
    }

    #[test]
    fn rounding_works_for_rounded_numbers() {
        assert_eq!(round_weight(10, 100.0, false), 10);
        assert_eq!(round_weight(15, 100.0, false), 15);
    }

    #[test]
    fn rounding_works_in_between_rounded_numbers() {
        assert_eq!(round_weight(11, 100.0, false), 10);
        assert_eq!(round_weight(12, 100.0, false), 10);
        assert_eq!(round_weight(13, 100.0, false), 15);
        assert_eq!(round_weight(14, 100.0, false), 15);
    }

    #[test]
    fn rounding_works_with_percentage_changes() {
        assert_eq!(round_weight(100, 50.0, false), 50);
        assert_eq!(round_weight(101, 50.0, false), 50);
        assert_eq!(round_weight(102, 50.0, false), 50);
        assert_eq!(round_weight(103, 50.0, false), 50);
        assert_eq!(round_weight(104, 50.0, false), 50);
    }

    #[test]
    fn rounding_works_with_percentage_changes_to_nearest_5() {
        assert_eq!(round_weight(105, 50.0, false), 55);
        assert_eq!(round_weight(106, 50.0, false), 55);
        assert_eq!(round_weight(107, 50.0, false), 55);
        assert_eq!(round_weight(108, 50.0, false), 55);
        assert_eq!(round_weight(109, 50.0, false), 55);
        assert_eq!(round_weight(110, 50.0, false), 55);
    }
}
