FROM docker.io/library/rust:latest

RUN USER=root cargo new --bin rust-app
WORKDIR ./rust-app

COPY Cargo.toml         Cargo.toml
COPY Makefile.toml      Makefile.toml
COPY index.html         index.html
COPY pkg                pkg
COPY Cargo.lock         build/Cargo.lock
COPY README.md          README.md
COPY src                src

RUN useradd -m appuser \
    && chown -R appuser /rust-app

USER appuser
WORKDIR /rust-app

RUN cargo install cargo-make \
    && cargo make build

expose 8000

CMD cargo make serve

