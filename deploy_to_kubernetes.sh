#!/bin/env bash

podman login quay.io

podman build -t toysite -f dockerfile

podman tag localhost/toysite quay.io/toyingaround/toysite:latest

podman push quay.io/toyingaround/toysite:latest

kubectl rollout restart deployment/toysite
